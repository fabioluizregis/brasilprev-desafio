# Projeto de Automação - Desafio Brasilprev

Projeto de automação, construído com Ruby + HTTParty.

## O que precisa para executar o projeto:

Primeiramente, precisará da API do desafio do projeto Brasilprev na máquina local e deixar rodando em "background" com o comando abaixo:

```bash
mvn spring-boot:run
```

Após isto, precisará ter o Ruby instalado na máquina.

Com o Ruby instalado na máquina e com esse projeto clonado em seu computador, precisará rodar os comandos abaixo para atualizar as bibliotecas do projeto:

```bash
gem install bundler
bundle install
```

## Executando o projeto
Com a API rodando e o projeto atualizado, agora é só rodar o comando abaixo:

```bash
cucumber
```

Pronto, agora seu projeto deverá executar e gerar um arquivo "report.html" na raiz do projeto.
Este arquivo poderá ser aberto em qualquer navegador de sua preferência.