class Pessoas
    include HTTParty
    base_uri "http://localhost:8080"

    #buscar uma pessoa pelo ddd e telefone
    def getBuscarPessoa(ddd, telefone)
        self.class.get("/pessoas/#{ddd}/#{telefone}")
    end  

    def postCadastrarPessoa(codigo, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, ddd, numero_telefone)

        @header = {'Content-Type' => 'application/json'}

        body = {
                    "codigo": codigo,
                    "nome": nome,
                    "cpf": cpf,
                    "enderecos": [
                                    {
                                        "logradouro": logradouro,
                                        "numero": numero,
                                        "complemento": complemento,
                                        "bairro": bairro,
                                        "cidade": cidade,
                                        "estado": estado
                                    }
                                ],
                    "telefones": [
                                    {
                                    "ddd": ddd,
                                    "numero": numero_telefone
                                    }
                                ]
                }
        @body = JSON.generate(body)

        self.class.post("/pessoas",:body => @body,:headers => @header)
    end
end