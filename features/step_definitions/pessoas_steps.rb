Dado('que tenho {string} e {string} da pessoa que desejo consultar') do |ddd, telefone|
    @ddd = ddd
    @telefone = telefone
end

Dado('que tenho o {string}, {string} e {string} para o cadastro') do |codigo, nome, cpf|
    @codigo = codigo.to_i
    @nome = nome
    @cpf = cpf
end

Dado('tenho o endereço com {string} {string} {string} {string} {string} {string}') do |logradouro, numero, complemento, bairro, cidade, estado|     
    @logradouro = logradouro
    @numero = numero.to_i
    @complemento = complemento
    @bairro = bairro
    @cidade = cidade
    @estado = estado
end
  
Dado('tenho o telefone de contato com {string} {string}') do |ddd, numero_telefone|
    @ddd = ddd
    @numero_telefone = numero_telefone
end

Quando('realizo um POST com esses dados') do
    @response = $pessoas.postCadastrarPessoa(@codigo, @nome, @cpf, @logradouro, @numero, @complemento, @bairro, @cidade, @estado, @ddd, @numero_telefone)
end

Quando('realizo um GET com esses dados') do
    @response = $pessoas.getBuscarPessoa(@ddd, @telefone)
end
  
Então('recebo o codigo de retorno {string}') do |codigo_retorno|
    expect(@response.code).to eq codigo_retorno.to_i
end


  

  
