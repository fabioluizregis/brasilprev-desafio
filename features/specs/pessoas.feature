#language: pt
  
Funcionalidade: Operações com Pessoas
Eu como um usuário da API do BrasilPrev
Quero acessar a área de pessoas
Para realizar operações consultas, cadastros e alterações
  
  Contexto: Ter acesso à API da Brasilprev
  
    @cadastro
    Esquema do Cenário: Cadastro de uma nova pessoa específica
        Dado que tenho o "<codigo>", "<nome>" e "<cpf>" para o cadastro
        E tenho o endereço com "<logradouro>" "<numero>" "<complemento>" "<bairro>" "<cidade>" "<estado>"
        E tenho o telefone de contato com "<ddd>" "<numero_telefone>"
        Quando realizo um POST com esses dados
        Então recebo o codigo de retorno "<codigo_retorno>"

        Exemplos:
            | codigo | nome            | cpf         | logradouro          | numero | complemento | bairro                | cidade    | estado | ddd | numero_telefone | codigo_retorno | 
            | 0      | Rafael Teixeira | 12345678909 | Rua Alexandre Dumas | 123    | Empresa     | Chacara Santo Antonio | São Paulo | SP     | 11  | 988244040       | 201            |
            | 1      | CPF Repetido    | 12345678909 | Rua Alexandre Dumas | 123    | Empresa     | Chacara Santo Antonio | São Paulo | SP     | 11  | 988244040       | 400            |
            | 2      | Telefone igual  | 98765432109 | Rua Alexandre Dumas | 123    | Empresa     | Chacara Santo Antonio | São Paulo | SP     | 11  | 988244040       | 400            |

    @consulta
    Esquema do Cenário: Consulta de uma pessoa especifica
        Dado que tenho "<ddd>" e "<telefone>" da pessoa que desejo consultar
        Quando realizo um GET com esses dados
        Então recebo o codigo de retorno "<codigo_retorno>"
            
            Exemplos:
                | ddd | telefone  | codigo_retorno | 
                | 11  | 988244040 | 200            | 
                | 11  | 999999999 | 404            |
                | 33  | 988244040 | 404            |
                |     | 988244040 | 404            |
                | 11  |           | 404            |
                | 11  | aaaaaaaaa | 404            |
                | aa  | 988244040 | 404            |